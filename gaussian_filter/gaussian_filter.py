from PIL import Image
from pylab import *
#from cv2 import *
#import Image
import math
import numpy
import matplotlib.pyplot as plt
#import cv2

GAUSSIAN_KERNEL = numpy.array([[1,4,7,4,1],
							   [4,16,26,16,4],
							   [7,26,41,26,7],
							   [4,16,26,16,4],
							   [1,4,7,4,1]])

'''

def separate_RGB(original_array,m):
	list = []
	num = len(original_array)
	for i in original_array:
		for j in i:
			list.append(j[m])
	#print num
	array = numpy.array(list)
	array2 = array.reshape((num,num))
	return array2
'''
 
def Gaussian_filter(input_array):
	[i,j] = input_array.shape
	output_array = input_array[:]
	m = 2
	while m < i-2:
		n = 2
		while n < j-2:
			block_array = input_array[m-2:m+3,n-2:n+3]
			gaussian_block = block_array*GAUSSIAN_KERNEL
			output_array[m,n] = sum(gaussian_block)/273
			n += 1
		m += 1
	return output_array

#	[]
#original_array = array(Image.open('Dragon.bmp'))
original_array = array(Image.open('baidu_gray_noise.bmp').convert('L'))

gaussian_array = Gaussian_filter(original_array)
gaussian_final_array = gaussian_array.astype(numpy.uint8)
result_picture = Image.fromarray(gaussian_final_array)
result_picture.save('baidu_gray_noise_gaussion.bmp')





'''
R_array = separate_RGB(original_array,0)
G_array = separate_RGB(original_array,1)
B_array = separate_RGB(original_array,2)
R_picture = Image.fromarray(R_array)
R_picture.save('Dragon_R.bmp')
G_picture = Image.fromarray(G_array)
G_picture.save('Dragon_G.bmp')
B_picture = Image.fromarray(B_array)
B_picture.save('Dragon_B.bmp')

print original_array
print R_array
'''