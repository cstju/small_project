from PIL import Image
from pylab import *
#from cv2 import *
#import Image
from numpy import random
import math
import random
import numpy
import matplotlib.pyplot as plt
#import cv2
def plus_noise(input_array):
	[i,j] = input_array.shape
	noise_array = numpy.zeros([i,j])
	m = 0
	while m<i:
		n = 0
		while n<j:
			noise_array[m][n] = random.gauss(-20,10)
			n += 1
		m += 1
	output_array = input_array + noise_array
	return output_array




original_array = array(Image.open('baidu.bmp').convert('L'))
original_picture = Image.fromarray(original_array)
original_picture.save('baidu_gray.bmp')


plus_noise_array = plus_noise(original_array)
plus_noise_final_array = plus_noise_array.astype(numpy.uint8)
result_picture = Image.fromarray(plus_noise_final_array)
result_picture.save('baidu_gray_noise.bmp')
