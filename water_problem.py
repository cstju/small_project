# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-04-21 21:37:16
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-04-23 12:30:03

import numpy as np
import time
import scipy as sp
import random,math

def water_problem(wc1,wc2):
	m = 0
	n = 0
	result = set()
	result.add(wc1)
	result.add(wc2)
	while m<=10:
		while n<=10:
			temp1 = m*wc1-n*wc2
			temp2 = n*wc2-m*wc1
			if temp1>=0 and temp1<= wc1+wc2:
				result.add(temp1)
			if temp2>=0 and temp2<= wc1+wc2:
				result.add(temp2)
			n += 1
		n = 0
		m+=1
	result.add(wc1+wc2)
	return result

print water_problem(4,9) 