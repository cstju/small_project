import numpy
import math
def array_to_list_Z(array):
	lis = []
	m = 0
	while m < 8:
		if m%2 == 0:
			i = m
			j = 0
			while j<=m:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = 0
			j = m
			while i<=m:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	m = 8
	while m < 15:
		if m%2 == 0:
			i = 7
			j = m-7
			while j<=7:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = m-7
			j = 7
			while i<=7:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	return lis
c = numpy.array([[15,0,-1,0,0,0,0,0],
				 [-2,-1,0,0,0,0,0,0],
				 [-1,-1,0,0,0,0,0,0],
				 [-1,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0]])
c_list = array_to_list_Z(c)
print c_list