from PIL import Image
from pylab import *
#from cv2 import *
#import Image
import math
import numpy
import matplotlib.pyplot as plt
#import cv2
Y_QUANTIZATION_CONSTANT= numpy.array([[16,11,10,16,24,40,51,61],
								      [12,12,14,19,26,58,60,55],
								      [14,13,16,24,40,57,69,56],
								      [14,17,22,29,51,87,80,62],
								      [18,22,37,56,68,109,103,77],
								      [24,35,55,64,81,104,113,92],
								      [49,64,78,87,103,121,120,101],
								      [72,92,95,98,112,100,103,99]])
DC_DICTIONARY = {0:00,
				 1:010,
				 2:011,
				 3:100,
				 4:101,
				 5:110,
				 6:1110,
				 7:11110,
				 8:111110,
				 9:1111110,
				 10:11111110,
				 11:111111110}
AC_DICTIONARY = {'0,0':1010,
				 '0,1':'00',
				 '0,2':01,
				 '0,3':100,
				 '0,4':1011,
				 '0,5':11010,
				 '0,6':1111000,
				 '0,7':11111000,
				 '1,1':1100,
				 '1,2':11011,
				 '1,3':1111001,
				 '1,4':111110110,
				 '1,5':11111110110,
				 '1,6':1111111110000100,
				 '1,7':1111111110000101,
				 '2,1':11100,
				 '2,2':11111001,
				 '2,3':1111110111,
				 '2,4':111111110100,
				 '2,5':1111111110001001,
				 '2,6':1111111110001010,
				 '2,7':1111111110001011,
				 '3,1':111010,
				 '3,2':111110111,
				 '3,3':111111110101,
				 '3,4':1111111110001111,
				 '3,5':1111111110010000,
				 '3,6':1111111110010001,
				 '3,7':1111111110010010,
				 '4,1':111011,
				 '4,2':1111111000,
				 '4,3':1111111110010110,
				 '4,4':1111111110010111,
				 '4,5':1111111110011000,
				 '4,6':1111111110011001,
				 '4,7':1111111110011010,
				 '5,1':1111010,
				 '5,2':11111110111,
				 '5,3':1111111110011110,
				 '5,4':1111111110011111,
				 '5,5':1111111110100000,
				 '5,6':1111111110100001,
				 '5,7':1111111110100010,
				 '6,1':1111011,
				 '6,2':111111110110,
				 '6,3':1111111110100110,
				 '6,4':1111111110100111,
				 '6,5':1111111110101000,
				 '6,6':1111111110101001,
				 '6,7':1111111110101010,
				 '7,1':11111010,
				 '7,2':111111110111,
				 '7,3':1111111110101110,
				 '7,4':1111111110101111,
				 '7,5':1111111110110000,
				 '7,6':1111111110110001,
				 '7,7':1111111110110010,
				 '8,1':111111000,
				 '8,2':111111111000000,
				 '8,3':1111111110110110,
				 '8,4':1111111110110111,
				 '8,5':1111111110111000,
				 '8,6':1111111110111001,
				 '8,7':1111111110111010,
				 '9,1':111111001,
				 '9,2':1111111110111110,
				 '9,3':1111111110111111,
				 '9,4':1111111111000000,
				 '9,5':1111111111000001,
				 '9,6':1111111111000010,
				 '9,7':1111111111000011,
				 '10,1':111111010,
				 '11,1':1111111001,
				 '12,1':1111111010,
				 '13,1':11111111000,
				 '14,1':1111111111101011,
				 '15,1':1111111111110101,
				 }
				 
				 #'16,1':1111111111111101,
				 #'17,1':1111111111111110,
				 #'19,1':1111111111111011,
				 #'20,1':1111111111111010,
				 #'21,1':1111111111111001,
				 #'25,1':1111111111111100,
				 #'39,1':
				 
				 
################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################
############################ENCODE##############################
################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################

##########################################################
def change_into_8x8(array):
	array_row_number = array.shape[0]
	array_column_number = array.shape[1]
	block_row_number = array_row_number/8
	block_column_number = array_column_number/8
	first_array = array.reshape((block_row_number,8,array_column_number))
	#is_first_block = True
	#i = 0
	final_array = []
	for second_array in first_array:
		second_array = zip(*second_array)
		second_array = numpy.array(second_array)
		third_array = second_array.reshape(block_column_number,8,8)
		for fourth_array in third_array:
			fourth_array = zip(*fourth_array)
			fourth_array = numpy.array(fourth_array)
			final_array.append(fourth_array)
			#i = i+1
			'''
			if is_first_block:
				final_array  = fourth_array
				is_first_block = False
			else:
				final_array = numpy.vstack((final_array,fourth_array))
			'''
			#print fourth_array
			#i += 1
			#print i
	final_array = numpy.array(final_array)
	return final_array
##########################################################
def FDCT(array):
	array = array - numpy.ones([8,8])*128
	fdct_array = numpy.zeros([8,8])
	u = 0
	while u < 8 :
		v = 0
		while v < 8:
			i = 0
			while i < 8:
				j = 0
				while j < 8:
					fdct_array[u][v] += array[i][j]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					j += 1
				i += 1
			v += 1
		u += 1	
	fdct_array = 1.0/4.0*fdct_array
	i = 0
	while i<8:
		fdct_array[0][i] = fdct_array[0][i]/math.sqrt(2)
		fdct_array[i][0] = fdct_array[i][0]/math.sqrt(2)
		i = i+1
	return fdct_array

def quantization(array,quantization_constant):
	 quantization_array = array/quantization_constant
	 round_quantization_array = numpy.zeros([8,8])
	 i = 0
	 while i < 8:
	 	j = 0
	 	while j< 8:
	 		round_quantization_array[i][j] = int(round(quantization_array[i][j]))
	 		j += 1
	 	i += 1
	 return round_quantization_array

def FDCT_quantization(array,quantization_constant):
	FDCT_quantization_list = []
	for i in array:
		fdct_i = FDCT(i)
		quantization_i = quantization(fdct_i,quantization_constant)
		FDCT_quantization_list.append(quantization_i)
	return numpy.array(FDCT_quantization_list)
##########################################################
def array_to_list_Z(array):
	lis = []
	m = 0
	while m < 8:
		if m%2 == 0:
			i = m
			j = 0
			while j<=m:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = 0
			j = m
			while i<=m:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	m = 8
	while m < 15:
		if m%2 == 0:
			i = 7
			j = m-7
			while j<=7:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = m-7
			j = 7
			while i<=7:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	
	#you can compress the list by only saving the tops elements in the list
	
	a = 0
	while a < 64:
		if a > 21:
			lis[a] = 0.0
		a += 1
	
	return lis
	
	#return numpy.array(lis)

def transform_into_z(array):
	z_list = []
	for i in array:
		z_i = array_to_list_Z(i)
		z_list.append(z_i)
	return z_list
	#return numpy.array(z_list)


##########################################################
def diff_to_ssss(diff):
	if diff == 0:
		ssss = 0
	elif diff==-1 or diff==1:
		ssss = 1
	elif diff==-3 or diff==3 or diff==-2 or diff==2:
		ssss = 2
	elif -7<=diff<=-4 or 4<=diff<=7:
		ssss = 3 
	elif -15<=diff<=-8 or 8<=diff<=15:
		ssss = 4
	elif -31<=diff<=-16 or 16<=diff<=31:
		ssss = 5 
	elif -63<=diff<=-32 or 32<=diff<=63:
		ssss = 6 
	elif -127<=diff<=-64 or 64<=diff<=127:
		ssss = 7 
	elif -255<=diff<=-128 or 128<=diff<=255:
		ssss = 8 
	elif -511<=diff<=-256 or 256<=diff<=511:
		ssss = 9 
	elif -1023<=diff<=-512 or 512<=diff<=1023:
		ssss = 10 
	elif -2047<=diff<=-1024 or 1024<=diff<=2047:
		ssss = 11
	return ssss

def Middle_Signal(list1):
	diff = list1[0]
	ssss = diff_to_ssss(diff)
	list2 = []
	list2.append(ssss)
	list2.append(diff)
	lis = []
	lis.append(list2)
	i = 1
	j = 0
	while i<64:
		if list1[i] == 0 :
			j += 1
		else:
			list3 = []
			rrrr = j
			ssss = diff_to_ssss(list1[i])
			ac = int(list1[i])
			rrrrssss = str(rrrr)+','+str(ssss)
			list3.append(rrrrssss)
			list3.append(ac)
			lis.append(list3)
			j = 0
		i += 1
	lis.append(['0,0'])
	return lis

def transform_into_middle_signal(list1):
	middle_signal_list = []
	for i in list1:
		middle_signal_i = Middle_Signal(i)
		middle_signal_list.append(middle_signal_i)
	return middle_signal_list
##########################################################
def Huffman(list1):
	list2 = list1[:]
	dc = list2.pop(0)
	final_str = ''
	dc_len = DC_DICTIONARY[dc[0]]
	final_str = final_str+str(dc_len)
	dc_bin = format(int(dc[1]),'b')
	final_str = final_str+dc_bin
	for ac in list2:
		if ac[0] == '0,0':
			final_str = final_str+str(AC_DICTIONARY[ac[0]])
			break
		else:
			ac_huf = str(AC_DICTIONARY[ac[0]])
			ac_bin = format(ac[1],'b')
			final_str = final_str + ac_huf
			final_str = final_str + ac_bin
	return final_str
def transform_into_huffman(list1):
	huffman_str = ''
	for i in list1:
		final_str = Huffman(i)
		huffman_str = huffman_str + final_str
	return huffman_str
##########################################################
#use the function 'imread' can get the matix of image
original_array = array(Image.open('01_SUV.bmp').convert('L'))
#original_array = imread('01_SUV.bmp')
print 'original_array:',
print  original_array
#print 'original_array.shape: ',original_array.shape

##########################################################
#transform the matix(64x64) to matix(64x8x8), every block is a matix(8x8)
#the total of matix is 64
block_array = change_into_8x8(original_array)
#print 'type(block_array): ',type(block_array)
#print 'block_array: ',block_array
#print 'block_array.shape: ',block_array.shape

##########################################################
#take the DCT transform and quantization
FDCT_array = FDCT_quantization(block_array,Y_QUANTIZATION_CONSTANT)
#print 'type(FDCT_array): ',type(FDCT_array)
#print 'FDCT_array[0]: '
#print  FDCT_array[0]
#print 'FDCT_array.shape: ',FDCT_array.shape
#print 'type(FDCT_array[11]): ',type(FDCT_array[11])
#print 'FDCT_array[11]: '
#print  FDCT_array[11]
#print 'FDCT_array[11].shape: ',FDCT_array[11].shape

##########################################################
#transform the matix into list by Z 
z_array = transform_into_z(FDCT_array)
#print 'type(z_array): ',type(z_array)
#print 'len(z_array): ',len(z_array)
#print 'z_array: ',z_array
#print 'type(z_array[13]): ',type(z_array[13])
#print 'len(z_array[13]): ',len(z_array[13])
#print 'z_array[13]: ',z_array[13]

##########################################################
#transform the z_list into middle_signal_array
middle_signal_array = transform_into_middle_signal(z_array)
#print 'type(middle_signal_array): ',type(middle_signal_array)
#print 'len(middle_signal_array): ',len(middle_signal_array)
#print 'middle_signal_array: ',middle_signal_array
#print 'type(middle_signal_array[44]): ',type(middle_signal_array[44])
#print 'len(Cb_middle_signal_array[44]): ',len(middle_signal_array[44])
#print 'middle_signal_array[44]: ',middle_signal_array[44]

##########################################################
huffman_output = transform_into_huffman(middle_signal_array)
after_encode = len(huffman_output)
#print huffman_output
print after_encode

##########################################################
#write the Y,Cb,Cr in the encode_result.txt
f = open('encode_result.txt','w+')
f.write(str(huffman_output))
f.close()

################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################
############################DECODE##############################
################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################

################################################################

def against_array_to_list_Z(list1):
	lis = numpy.zeros([8,8])
	m = 0
	n = 0
	#the value of m must less than 8
	#in other words , the low right corner of the matix is 0 
	while m < 4:
		if m%2 == 0:
			i = m
			j = 0				
			while j<=m:
				lis[i][j] = list1[n]
				n += 1
				j += 1
				i = m-j
		else :
			i = 0
			j = m
			while i<=m:
				lis[i][j] = list1[n]
				n += 1
				i += 1
				j = m-i
		m += 1
	return lis


def against_transform_z(list1):
	matix_list = []
	for i in list1:
		matix_i = against_array_to_list_Z(i)
		matix_list.append(matix_i)
	return matix_list

################################################################
def against_quantization_IDCT(array,quantization_constant):
	array = array*quantization_constant
	idct_array = numpy.zeros([8,8])
	i = 0
	while i<8:
		array[0][i] = array[0][i]/math.sqrt(2)
		array[i][0] = array[i][0]/math.sqrt(2)
		i = i+1
	i = 0
	while i < 8 :
		j = 0
		while j < 8:
			u = 0
			while u < 8:
				v = 0
				while v < 8:
					idct_array[i][j] += array[u][v]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					v += 1
				u += 1
			j += 1
		i += 1	
	idct_array = 1.0/4.0*idct_array+numpy.ones([8,8])*128
	round_quantization_array = numpy.zeros([8,8])
	m = 0
	while m < 8:
	 	n = 0
	 	while n< 8:
	 		round_quantization_array[m][n] = int(round(idct_array[m][n]))
	 		n += 1
	 	m += 1
	return round_quantization_array

def against_transform_block_matix(list1,quantization_constant):
	matix_list = []
	for i in list1:
		matix_i = against_quantization_IDCT(i,quantization_constant)
		matix_list.append(matix_i)
	return matix_list

################################################################
def against_change_into_8x8(list1):
	L = len(list1)
	l = 0
	final_array = numpy.zeros([8*math.sqrt(L),8*math.sqrt(L)])
	m = 0
	n = 0
	while l < L:
		array1 = list1[l]
		i = 0
		while i <8:
			j = 0
			while j < 8:
				
				temp = array1[i][j]
				final_array[i+m][j+n] = temp
				j += 1
			i += 1
		i = 0
		j = 0
		l += 1
		n += 8
		if l % math.sqrt(L) ==0:
			m += 8
			n = 0
	return final_array

################################################################
def array_normal(array):
	array1 = array
	m = array1.shape[0]
	i = 0
	while i <m :
		j = 0
		while j <m :
			if array1[i][j] > 255:
				array1[i][j] = 255
			elif array1[i][j] < 0:
				array1[i][j] = 0
			j += 1
		i += 1
	return array1


################################################################
#transform the 65-z list into 8x8 matix
against_FDCT_array = against_transform_z(z_array)
#print 'type(against_FDCT_array): ',type(against_FDCT_array)
#print 'len(z_array): ',len(z_array)
#print 'against_FDCT_array[0]: '
#print  against_FDCT_array[0]
#print 'type(against_FDCT_array[0]): ',type(against_FDCT_array[0])
#print 'against_FDCT_array[0].shape:',against_FDCT_array[0].shape

################################################################
#transform the FDCT 8x8 matix list into the original 8x8 matix
against_block_array = against_transform_block_matix(against_FDCT_array,Y_QUANTIZATION_CONSTANT)
#print against_block_array[0]
#print type(against_block_array[0])
#print type(against_block_array)
#print against_block_array[0].shape
#print len(against_block_array)

#print against_block_array[0]
#print against_block_array[3]
#print against_block_array[12]
#print against_block_array[15]

################################################################
#transform the original 8x8 matix into the orginal shape matix
against_original_array = against_change_into_8x8(against_block_array)
#print against_original_array

################################################################
against_normal_array = array_normal(against_original_array)

################################################################
#transform the matix's type from float64 to uint8
against_final_array = against_normal_array.astype(uint8)
print against_final_array.dtype
print against_final_array
cv2.imwrite('01_SUV_output.bmp', against_final_array)
#imwrite('01_SUV_output.png',against_final_array)
#img = Image.open('01_SUV_output.png')
#img = img.convert('L')
#img.save('01_SUV_output2.bmp','bmp')

'''
#print type(block_array[63])
print block_array[63]
#print block_array[63]-numpy.ones([8,8])*128
#print FDCT(block_array[63]-128)
print FDCT_array[63]
print z_array[63]
print middle_signal_array[63]
print against_array_to_list_Z(z_array[63])
#print against_array_to_list_Z(z_array[63])*Y_QUANTIZATION_CONSTANT
print against_quantization_IDCT(FDCT_array[63], Y_QUANTIZATION_CONSTANT)
a = numpy.array([1,2,3])
print a-5-4
'''