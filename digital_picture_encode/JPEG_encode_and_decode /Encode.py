from PIL import Image
from pylab import *
#from cv2 import *
#import Image
import math
import numpy
#import cv2
Y_QUANTIZATION_CONSTANT= numpy.array([[16,11,10,16,24,40,51,61],
								      [12,12,14,19,26,58,60,55],
								      [14,13,16,24,40,57,69,56],
								      [14,17,22,29,51,87,80,62],
								      [18,22,37,56,68,109,103,77],
								      [24,35,55,64,81,104,113,92],
								      [49,64,78,87,103,121,120,101],
								      [72,92,95,98,112,100,103,99]])
CbCr_QUANTIZATION_CONSTANT = numpy.array([[17,18,24,47,99,99,99,99],
									      [18,21,26,66,99,99,99,99],
									      [24,26,56,99,99,99,99,99],
									      [47,66,99,99,99,99,99,99],
									      [99,99,99,99,99,99,99,99],
									      [99,99,99,99,99,99,99,99],
									      [99,99,99,99,99,99,99,99],
									      [99,99,99,99,99,99,99,99]])

################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################
############################ENCODE##############################
################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################

##########################################################
def separate_RGB(orginial_array,m):
	list = []
	num = len(orginial_array)
	for i in orginial_array:
		for j in i:
			list.append(j[m])
	#print num
	array = numpy.array(list)
	array2 = array.reshape((num,num))
	return array2
	#return orginial_array[m]*256//1
##########################################################
def change_into_8x8(array):
	array_row_number = array.shape[0]
	array_column_number = array.shape[1]
	block_row_number = array_row_number/8
	block_column_number = array_column_number/8
	first_array = array.reshape((block_row_number,8,array_column_number))
	#is_first_block = True
	#i = 0
	final_array = []
	for second_array in first_array:
		second_array = zip(*second_array)
		second_array = numpy.array(second_array)
		third_array = second_array.reshape(block_column_number,8,8)
		for fourth_array in third_array:
			fourth_array = zip(*fourth_array)
			fourth_array = numpy.array(fourth_array)
			final_array.append(fourth_array)
			#i = i+1
			'''
			if is_first_block:
				final_array  = fourth_array
				is_first_block = False
			else:
				final_array = numpy.vstack((final_array,fourth_array))
			'''
			#print fourth_array
			#i += 1
			#print i
	final_array = numpy.array(final_array)
	return final_array
##########################################################
def FDCT(array):
	array = array - 128
	fdct_array = numpy.zeros([8,8])
	u = 0
	while u < 8 :
		v = 0
		while v < 8:
			i = 0
			while i < 8:
				j = 0
				while j < 8:
					fdct_array[u][v] += array[i][j]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					j += 1
				i += 1
			v += 1
		u += 1	
	fdct_array = 1.0/4.0*fdct_array
	i = 0
	while i<8:
		fdct_array[0][i] = fdct_array[0][i]/math.sqrt(2)
		fdct_array[i][0] = fdct_array[i][0]/math.sqrt(2)
		i = i+1
	return fdct_array

def quantization(array,quantization_constant):
	 quantization_array = array/quantization_constant
	 round_quantization_array = numpy.zeros([8,8])
	 i = 0
	 while i < 8:
	 	j = 0
	 	while j< 8:
	 		round_quantization_array[i][j] = int(round(quantization_array[i][j]))
	 		j += 1
	 	i += 1
	 return round_quantization_array

def FDCT_quantization(array,quantization_constant):
	FDCT_quantization_list = []
	for i in array:
		fdct_i = FDCT(i)
		quantization_i = quantization(fdct_i,quantization_constant)
		FDCT_quantization_list.append(quantization_i)
	return numpy.array(FDCT_quantization_list)
##########################################################
def array_to_list_Z(array):
	lis = []
	m = 0
	while m < 8:
		if m%2 == 0:
			i = m
			j = 0
			while j<=m:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = 0
			j = m
			while i<=m:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	m = 8
	while m < 15:
		if m%2 == 0:
			i = 7
			j = m-7
			while j<=7:
				lis.append(array[i][j])
				j += 1
				i = m-j
		elif m%2 == 1:
			i = m-7
			j = 7
			while i<=7:
				lis.append(array[i][j])
				i += 1
				j = m-i
		m += 1
	
	#you can compress the list by only saving the tops elements in the list
	'''
	a = 0
	while a < 64:
		if a > 10:
			lis[a] = 0.0
		a += 1
	'''
	return lis
	
	#return numpy.array(lis)

def transform_into_z(array):
	z_list = []
	for i in array:
		z_i = array_to_list_Z(i)
		z_list.append(z_i)
	return z_list
	#return numpy.array(z_list)

'''
##########################################################
def diff_to_ssss(diff):
	if diff == 0:
		ssss = 0
	elif diff==-1 or diff==1:
		ssss = 1
	elif diff==-3 or diff==3 or diff==-2 or diff==2:
		ssss = 2
	elif -7<=diff<=-4 or 4<=diff<=7:
		ssss = 3 
	elif -15<=diff<=-8 or 8<=diff<=15:
		ssss = 4
	elif -31<=diff<=-16 or 16<=diff<=31:
		ssss = 5 
	elif -63<=diff<=-32 or 32<=diff<=63:
		ssss = 6 
	elif -127<=diff<=-64 or 64<=diff<=127:
		ssss = 7 
	elif -255<=diff<=-128 or 128<=diff<=255:
		ssss = 8 
	elif -511<=diff<=-256 or 256<=diff<=511:
		ssss = 9 
	elif -1023<=diff<=-512 or 512<=diff<=1023:
		ssss = 10 
	elif -2047<=diff<=-1024 or 1024<=diff<=2047:
		ssss = 11
	return ssss

def Middle_Signal(list1):
	diff = list1[0]
	ssss = diff_to_ssss(diff)
	list2 = []
	list2.append(ssss)
	list2.append(diff)
	lis = []
	lis.append(list2)
	i = 1
	j = 0
	while i<64:
		if list1[i] == 0 :
			j += 1
		else:
			list3 = []
			rrrr = j
			ssss = diff_to_ssss(list1[i])
			ac = list1[i]
			list3.append(rrrr)
			list3.append(ssss)
			list3.append(ac)
			lis.append(list3)
			j = 0
		i += 1
	lis.append([0,0])
	return lis
	
def transform_into_middle_signal(list1):
	middle_signal_list = []
	for i in list1:
		middle_signal_i = Middle_Signal(i)
		middle_signal_list.append(middle_signal_i)
	return middle_signal_list
'''

##########################################################
#use the function 'imread' can get the matix of image
original_array = imread('rss.bmp')
#print 'orginial_array.shape: ',orginial_array.shape

##########################################################
#separate the R,G,B from the image's matix,get three matix
R_array = separate_RGB(original_array,0)
G_array = separate_RGB(original_array,1)
B_array = separate_RGB(original_array,2)
#print 'R_array.shape: ',R_array.shape
#print 'R_array: ',R_array
#print 'G_array: ',G_array
#print 'B_array: ',B_array

##########################################################
#transform the color model. From RGB to YCbCr
Y_original_array = (0.299*R_array + 0.587*G_array + 0.114*B_array)//1
Cb_original_array = (-0.1687*R_array - 0.3313*G_array + 0.5*B_array + 128)//1
Cr_original_array = (0.5*R_array - 0.418*G_array - 0.0813*B_array + 128)//1
#print 'Cr_original_array: ',Cr_original_array
#print 'Cr_original_array.shape: ',Cr_original_array.shape

##########################################################
# I don't know if I sample the YUV(use 4:2:0 or 4:1:1 or others)  
# when I decode how can I transform the YUV to RGB
# so I don't use sampling in this code
# Y_samlpe_list = Y_original_list
# Cb_sample_array = sample(Cb_original_array)
# Cr_sample_array = sample(Cr_original_array)

##########################################################
#transform the matix(64x64) to matix(64x8x8), every block is a matix(8x8)
#the total of matix is 64
Y_block_array = change_into_8x8(Y_original_array)
Cb_block_array = change_into_8x8(Cb_original_array)
Cr_block_array = change_into_8x8(Cr_original_array)
#print 'type(Y_block_array): ',type(Y_block_array)
#print 'Y_block_array: ',Y_block_array
#print 'Y_block_array.shape: ',Y_block_array.shape

##########################################################
#take the DCT transform and quantization
Y_FDCT_array = FDCT_quantization(Y_block_array,Y_QUANTIZATION_CONSTANT)
Cb_FDCT_array = FDCT_quantization(Cb_block_array,CbCr_QUANTIZATION_CONSTANT)
Cr_FDCT_array = FDCT_quantization(Cr_block_array,CbCr_QUANTIZATION_CONSTANT)
#print 'type(Cb_FDCT_array): ',type(Cb_FDCT_array)
#print 'Cb_FDCT_array[0]: '
#print  Cb_FDCT_array[0]
#print 'Cb_FDCT_array.shape: ',Cb_FDCT_array.shape
#print 'type(Cb_FDCT_array[11]): ',type(Cb_FDCT_array[11])
#print 'Cb_FDCT_array[11]: ',Cb_FDCT_array[11]
#print 'Cb_FDCT_array[11].shape: ',Cb_FDCT_array[11].shape

##########################################################
#transform the matix into list by Z 
Y_z_array = transform_into_z(Y_FDCT_array)
Cb_z_array = transform_into_z(Cb_FDCT_array)
Cr_z_array = transform_into_z(Cr_FDCT_array)
#print 'type(Cb_z_array): ',type(Cb_z_array)
#print 'len(Y_z_array): ',len(Y_z_array)
#print 'Cb_z_array: ',Cb_z_array
#print 'type(Cb_z_array[13]): ',type(Cb_z_array[13])
#print 'len(Y_z_array[13]): ',len(Y_z_array[13])
#print 'Y_z_array[13]: ',Y_z_array[13]
##########################################################
#transform the z_list into middle_signal_array
'''
Y_middle_signal_array = transform_into_middle_signal(Y_z_array)
Cb_middle_signal_array = transform_into_middle_signal(Cb_z_array)
Cr_middle_signal_array = transform_into_middle_signal(Cr_z_array)
#print 'type(Cb_middle_signal_array): ',type(Cb_middle_signal_array)
#print 'len(Cb_middle_signal_array): ',len(Cb_middle_signal_array)
#print 'Cb_middle_signal_array: ',Cb_middle_signal_array
#print 'type(Cb_middle_signal_array[44]): ',type(Cb_middle_signal_array[13])
#print 'len(Cb_middle_signal_array[44]): ',len(Cb_middle_signal_array[13])
#print 'Cb_middle_signal_array[44]: ',Cb_middle_signal_array[13]

##########################################################
#write the Y,Cb,Cr in the encode_result.txt
f = open('encode_result1.txt','w+')
f.write(str(Y_middle_signal_array))
f.write('\r')
f.write(str(Cb_middle_signal_array))
f.write('\r')
f.write(str(Cr_middle_signal_array))
f.close()
'''

################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################
############################DECODE##############################
################@@@@@@@@@@@@@@@@@@@@@@@@@@@@####################

################################################################
def against_array_to_list_Z(list1):
	lis = numpy.zeros([8,8])
	m = 0
	n = 0
	#the value of m must less than 8
	#in other words , the low right corner of the matix is 0 
	while m < 4:
		if m%2 == 0:
			i = m
			j = 0				
			while j<=m:
				lis[i][j] = list1[n]
				n += 1
				j += 1
				i = m-j
		else :
			i = 0
			j = m
			while i<=m:
				lis[i][j] = list1[n]
				n += 1
				i += 1
				j = m-i
		m += 1
	return lis


def against_transform_z(list1):
	matix_list = []
	for i in list1:
		matix_i = against_array_to_list_Z(i)
		matix_list.append(matix_i)
	return matix_list

################################################################
def against_quantization_IDCT(array,quantization_constant):
	array = array*quantization_constant
	idct_array = numpy.zeros([8,8])
	i = 0
	while i<8:
		array[0][i] = array[0][i]/math.sqrt(2)
		array[i][0] = array[i][0]/math.sqrt(2)
		i = i+1
	i = 0
	while i < 8 :
		j = 0
		while j < 8:
			u = 0
			while u < 8:
				v = 0
				while v < 8:
					idct_array[i][j] += array[u][v]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					v += 1
				u += 1
			j += 1
		i += 1	
	idct_array = 1.0/4.0*idct_array+128
	round_quantization_array = numpy.zeros([8,8])
	m = 0
	while m < 8:
	 	n = 0
	 	while n< 8:
	 		round_quantization_array[m][n] = int(round(idct_array[m][n]))
	 		n += 1
	 	m += 1
	return round_quantization_array

def against_transform_block_matix(list1,quantization_constant):
	matix_list = []
	for i in list1:
		matix_i = against_quantization_IDCT(i,quantization_constant)
		matix_list.append(matix_i)
	return matix_list

################################################################
def against_change_into_8x8(list1):
	L = len(list1)
	l = 0
	final_array = numpy.zeros([8*math.sqrt(L),8*math.sqrt(L)])
	m = 0
	n = 0
	while l < L:
		array1 = list1[l]
		i = 0
		while i <8:
			j = 0
			while j < 8:
				temp = array1[i][j]
				final_array[i+m][j+n] = temp
				j += 1
			i += 1
		i = 0
		j = 0
		l += 1
		n += 8
		if l % math.sqrt(L) ==0:
			m += 8
			n = 0
	return final_array

################################################################
def array_normal(array):
	array1 = array
	m = array1.shape[0]
	print m
	i = 0
	while i <m :
		j = 0
		while j <m :
			if array1[i][j] > 255:
				array1[i][j] = 255
			elif array1[i][j] < 0:
				array1[i][j] = 0
			j += 1
		i += 1
	return array1


################################################################
#transform the 65-z list into 8x8 matix
against_Y_FDCT_array = against_transform_z(Y_z_array)
against_Cb_FDCT_array = against_transform_z(Cb_z_array)
against_Cr_FDCT_array = against_transform_z(Cr_z_array)
#print 'type(against_Cb_FDCT_array): ',type(against_Cb_FDCT_array)
#print 'len(Cb_z_array): ',len(Cb_z_array)
#print 'against_Cb_FDCT_array[0]: '
#print  against_Cb_FDCT_array[0]
#print 'type(against_Cb_FDCT_array[0]): ',type(against_Cb_FDCT_array[0])
#print 'against_Cb_FDCT_array[0].shape:',against_Cb_FDCT_array[0].shape

################################################################
#transform the FDCT 8x8 matix list into the original 8x8 matix
against_Y_block_array = against_transform_block_matix(against_Y_FDCT_array,Y_QUANTIZATION_CONSTANT)
against_Cb_block_array = against_transform_block_matix(against_Cb_FDCT_array,CbCr_QUANTIZATION_CONSTANT)
against_Cr_block_array = against_transform_block_matix(against_Cr_FDCT_array,CbCr_QUANTIZATION_CONSTANT)
#print against_Cb_block_array[0]
#print type(against_Cr_block_array[0])
#print type(against_Cr_block_array)
#print against_Cb_block_array[0].shape
#print len(against_Y_block_array)

#print against_Y_block_array[0]
#print against_Y_block_array[3]
#print against_Y_block_array[12]
#print against_Y_block_array[15]

################################################################
#transform the original 8x8 matix into the orginal shape YCbCr matix
against_Y_original_array = against_change_into_8x8(against_Y_block_array)
against_Cb_original_array = against_change_into_8x8(against_Cb_block_array)
against_Cr_original_array = against_change_into_8x8(against_Cr_block_array)

#print against_Y_original_array
#print against_Y_original_array.shape
################################################################
#transform YCbCr matix into RGB matix
against_R_array = (against_Y_original_array + 1.402*(against_Cr_original_array-128))//1
against_G_array = (against_Y_original_array - 0.34414*(against_Cb_original_array-128) - 0.71414*(against_Cr_original_array-128))//1
against_B_array = (against_Y_original_array + 1.772*(against_Cb_original_array-128))//1

################################################################
against_R_normal_array = array_normal(against_R_array)
against_G_normal_array = array_normal(against_G_array)
against_B_normal_array = array_normal(against_B_array)
print against_R_normal_array
print against_R_array.shape

################################################################
print orginial_array
print orginial_array.shape
#cv2.imwrite('rss1.bmp',orginial_array)
'''
B=open('rss2.bmp','w')
B.write(orginial_array)
B.close()
'''