import math
import random
import numpy
Y_QUANTIZATION_CONSTANT= numpy.array([[16,11,10,16,24,40,51,61],
								   [12,12,14,19,26,58,60,55],
								   [14,13,16,24,40,57,69,56],
								   [14,17,22,29,51,87,80,62],
								   [18,22,37,56,68,109,103,77],
								   [24,35,55,64,81,104,113,92],
								   [49,64,78,87,103,121,120,101],
								   [72,92,95,98,112,100,103,99]])
CrCb_QUANTIZATION_CONSTANT = numpy.array([[17,18,24,47,99,99,99,99],
									   [18,21,26,66,99,99,99,99],
									   [24,26,56,99,99,99,99,99],
									   [47,66,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99]])

def against_quantization_IDCT(array,quantization_constant):
	array = array*quantization_constant
	idct_array = numpy.zeros([8,8])
	i = 0
	while i<8:
		array[0][i] = array[0][i]/math.sqrt(2)
		array[i][0] = array[i][0]/math.sqrt(2)
		i = i+1
	i = 0
	while i < 8 :
		j = 0
		while j < 8:
			u = 0
			while u < 8:
				v = 0
				while v < 8:
					idct_array[i][j] += array[u][v]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					v += 1
				u += 1
			j += 1
		i += 1	
	idct_array = 1.0/4.0*idct_array+128
	'''
	i = 0
	while i<8:
		idct_array[0][i] = idct_array[0][i]/math.sqrt(2)
		idct_array[i][0] = idct_array[i][0]/math.sqrt(2)
		i = i+1
		'''
	round_quantization_array = numpy.zeros([8,8])
	m = 0
	while m < 8:
	 	n = 0
	 	while n< 8:
	 		round_quantization_array[m][n] = int(round(idct_array[m][n]))
	 		n += 1
	 	m += 1
	return round_quantization_array
c = numpy.array([[15,0,-1,0,0,0,0,0],
				 [-2,-1,0,0,0,0,0,0],
				 [-1,-1,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0],
				 [0,0,0,0,0,0,0,0]])
d = against_quantization_IDCT(c,Y_QUANTIZATION_CONSTANT)
print d
print type(d)
print d.shape
#idct_d = IDCT(d)

#print idct_d

