import math
import random
import numpy
DC_DICTIONARY = {0:00,
				 1:010,
				 2:011,
				 3:100,
				 4:101,
				 5:110,
				 6:1110,
				 7:11110,
				 8:111110,
				 9:1111110,
				 10:11111110,
				 11:111111110}
AC_DICTIONARY = {'0,0':1010,
				 '0,1':'00',
				 '0,2':01,
				 '0,3':100,
				 '0,4':1011,
				 '0,5':11010,
				 '0,6':1111000,
				 '0,7':11111000,
				 '1,1':1100,
				 '1,2':11011,
				 '1,3':1111001,
				 '1,4':111110110,
				 '1,5':11111110110,
				 '1,6':1111111110000100,
				 '1,7':1111111110000101,
				 '2,1':11100,
				 '2,2':11111001,
				 '2,3':1111110111,
				 '2,4':111111110100,
				 '2,5':1111111110001001,
				 '2,6':1111111110001010,
				 '2,7':1111111110001011,
				 '3,1':111010,
				 '3,2':111110111,
				 '3,3':111111110101,
				 '3,4':1111111110001111,
				 '3,5':1111111110010000,
				 '3,6':1111111110010001,
				 '3,7':1111111110010010,
				 '4,1':111011,
				 '4,2':1111111000,
				 '4,3':1111111110010110,
				 '4,4':1111111110010111,
				 '4,5':1111111110011000,
				 '4,6':1111111110011001,
				 '4,7':1111111110011010,
				 '5,1':1111010,
				 '5,2':11111110111,
				 '5,3':1111111110011110,
				 '5,4':1111111110011111,
				 '5,5':1111111110100000,
				 '5,6':1111111110100001,
				 '5,7':1111111110100010,
				 '6,1':1111011,
				 '6,2':111111110110,
				 '6,3':1111111110100110,
				 '6,4':1111111110100111,
				 '6,5':1111111110101000,
				 '6,6':1111111110101001,
				 '6,7':1111111110101010}
def diff_to_ssss(diff):
	if diff == 0:
		ssss = 0
	elif diff==-1 or diff==1:
		ssss = 1
	elif diff==-3 or diff==3 or diff==-2 or diff==2:
		ssss = 2
	elif -7<=diff<=-4 or 4<=diff<=7:
		ssss = 3 
	elif -15<=diff<=-8 or 8<=diff<=15:
		ssss = 4
	elif -31<=diff<=-16 or 16<=diff<=31:
		ssss = 5 
	elif -63<=diff<=-32 or 32<=diff<=63:
		ssss = 6 
	elif -127<=diff<=-64 or 64<=diff<=127:
		ssss = 7 
	elif -255<=diff<=-128 or 128<=diff<=255:
		ssss = 8 
	elif -511<=diff<=-256 or 256<=diff<=511:
		ssss = 9 
	elif -1023<=diff<=-512 or 512<=diff<=1023:
		ssss = 10 
	elif -2047<=diff<=-1024 or 1024<=diff<=2047:
		ssss = 11
	return ssss
def Middle_Signal(list1):
	diff = list1[0]
	ssss = diff_to_ssss(diff)
	list2 = []
	list2.append(ssss)
	list2.append(diff)
	lis = []
	lis.append(list2)
	i = 1
	j = 0
	while i<64:
		if list1[i] == 0 :
			j += 1
		else:
			list3 = []
			rrrr = j
			ssss = diff_to_ssss(list1[i])
			ac = list1[i]
			rrrrssss = str(rrrr)+','+str(ssss)
			list3.append(rrrrssss)
			list3.append(ac)
			lis.append(list3)
			j = 0
		i += 1
	lis.append(['0,0'])
	return lis

def Huffman(list1):
	list2 = list1[:]
	dc = list2.pop(0)
	final_str = ''
	dc_len = DC_DICTIONARY[dc[0]]
	final_str = final_str+str(dc_len)
	dc_bin = format(dc[1],'b')
	final_str = final_str+dc_bin
	#print final_str
	#print list2
	for ac in list2:
		if ac[0] == '0,0':
			final_str = final_str+str(AC_DICTIONARY[ac[0]])
			break
		else:
			ac_huf = str(AC_DICTIONARY[ac[0]])
			ac_bin = format(ac[1],'b')
			final_str = final_str + ac_huf
			final_str = final_str + ac_bin
	return final_str

c = [15, 0, -2, -1, -1, -1, 0, 0, -1, -1, 
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

middle_signal = Middle_Signal(c)
Huffman_signal = Huffman(middle_signal)
print Huffman_signal

print c
print type(c)
print middle_signal
'''
print len(middle_signal)
print type(middle_signal)
print middle_signal[1]
print len(middle_signal[1])
print type(middle_signal[1])
print len(middle_signal[-2])
print middle_signal[-2]
print middle_signal[-2][0]
print middle_signal[-2][0][0]
print type(middle_signal[-2][0][0])
print int(middle_signal[-2][0][0])
print type(int(middle_signal[-2][0][0]))
'''
'''
m = -5
print bin(m)
b = format(m,'b')
print b
x = int(b,2)
print x
print type(bin(m))
print type(bin(m))
'''
