import math
import random
import numpy
Y_QUANTIZATION_CONSTANT= numpy.array([[16,11,10,16,24,40,51,61],
								   [12,12,14,19,26,58,60,55],
								   [14,13,16,24,40,57,69,56],
								   [14,17,22,29,51,87,80,62],
								   [18,22,37,56,68,109,103,77],
								   [24,35,55,64,81,104,113,92],
								   [49,64,78,87,103,121,120,101],
								   [72,92,95,98,112,100,103,99]])
CrCb_QUANTIZATION_CONSTANT = numpy.array([[17,18,24,47,99,99,99,99],
									   [18,21,26,66,99,99,99,99],
									   [24,26,56,99,99,99,99,99],
									   [47,66,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99],
									   [99,99,99,99,99,99,99,99]])
def FDCT(array):
	array = array - 128
	fdct_array = numpy.zeros([8,8])
	u = 0
	while u < 8 :
		v = 0
		while v < 8:
			i = 0
			while i < 8:
				j = 0
				while j < 8:
					fdct_array[u][v] += array[i][j]*math.cos(((2*i+1)*u*math.pi)/16)*math.cos(((2*j+1)*v*math.pi)/16)
					j += 1
				i += 1
			v += 1
		u += 1	
	fdct_array = 1.0/4.0*fdct_array
	i = 0
	while i<8:
		fdct_array[0][i] = fdct_array[0][i]/math.sqrt(2)
		fdct_array[i][0] = fdct_array[i][0]/math.sqrt(2)
		i = i+1
	return fdct_array

def quantization(array,quantization_constant):
	 quantization_array = array/quantization_constant
	 round_quantization_array = numpy.zeros([8,8])
	 i = 0
	 while i < 8:
	 	j = 0
	 	while j< 8:
	 		round_quantization_array[i][j] = int(round(quantization_array[i][j]))
	 		j += 1
	 	i += 1
	 return round_quantization_array	
def against_quantization(array,quantization_constant):
	return array*quantization_constant
c = numpy.array([[139,144,149,153,155,155,155,155],
				 [144,151,153,156,159,156,156,156],
				 [150,155,160,163,158,156,156,156],
				 [159,161,162,160,160,159,159,159],
				 [159,160,161,162,162,155,155,155],
				 [161,161,161,161,160,157,157,157],
				 [162,162,161,163,162,157,157,157],
				 [162,162,161,161,163,158,158,158]])
fdct_c = FDCT(c)
quantization_c = quantization(fdct_c,Y_QUANTIZATION_CONSTANT)
#d = against_quantization(quantization_c,Y_QUANTIZATION_CONSTANT)
print c-128
print quantization_c
'''
print c[0][3]
print math.pi
print math.cos(math.pi)
print quantization_c
'''